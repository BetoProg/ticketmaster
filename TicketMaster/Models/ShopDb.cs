namespace TicketMaster.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public class ShopDb : DbContext
    {
        public ShopDb()
            : base("name=ShopDbContext")
        {
        }

        public virtual DbSet<PAY_PAYMENT> PAY_PAYMENT { get; set; }
        public virtual DbSet<PAY_TICKET> PAY_TICKET { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PAY_PAYMENT>()
                .Property(e => e.PAY_TOTAL)
                .HasPrecision(12, 2);

            modelBuilder.Entity<PAY_TICKET>()
                .Property(e => e.TIC_SUBTOTAL)
                .HasPrecision(12, 4);

            modelBuilder.Entity<PAY_TICKET>()
                .Property(e => e.TIC_TAX)
                .HasPrecision(12, 4);

            modelBuilder.Entity<PAY_TICKET>()
                .Property(e => e.TIC_TOTALAMOUNT)
                .HasPrecision(12, 4);

            modelBuilder.Entity<PAY_TICKET>()
                .HasMany(e => e.PAY_PAYMENT)
                .WithRequired(e => e.PAY_TICKET)
                .WillCascadeOnDelete(false);
        }

        public System.Data.Entity.DbSet<TicketMaster.Models.PaymentTicketViewModel> PaymentTicketViewModels { get; set; }
    }
}
