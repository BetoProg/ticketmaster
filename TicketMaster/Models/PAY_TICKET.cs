namespace TicketMaster.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAY_TICKET
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAY_TICKET()
        {
            PAY_PAYMENT = new HashSet<PAY_PAYMENT>();
        }

        [Key]
        [StringLength(20)]
        public string TIC_ID { get; set; }

        public short BRA_ID { get; set; }

        public int? DLY_ID { get; set; }

        public int TIC_TICKETSTATE { get; set; }

        [Required]
        [StringLength(20)]
        public string CUS_ID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TIC_SUBTOTAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TIC_TAX { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TIC_TOTALAMOUNT { get; set; }

        [StringLength(20)]
        public string TIC_SELLER { get; set; }

        public int GEN_CUR_ID { get; set; }

        public short GEN_TAX_ID { get; set; }

        public int PUT_ID { get; set; }

        public int? EXC_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAY_PAYMENT> PAY_PAYMENT { get; set; }
    }
}
