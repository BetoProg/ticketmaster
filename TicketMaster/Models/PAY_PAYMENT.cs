namespace TicketMaster.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAY_PAYMENT
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string TIC_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PAY_ID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PAY_TOTAL { get; set; }

        public DateTime PAY_PAYMENTDATE { get; set; }

        public int PAY_CANCELED { get; set; }

        public int CUR_ID { get; set; }

        [StringLength(100)]
        public string PAY_PINREFNUMBER { get; set; }

        public virtual PAY_TICKET PAY_TICKET { get; set; }
    }
}
