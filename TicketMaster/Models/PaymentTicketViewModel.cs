﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TicketMaster.Models
{
    public class PaymentTicketViewModel
    {
        [Key]
        public string tic_id { get; set; }
        public string  cliente { get; set; }
        public decimal monto { get; set; }
        public decimal total_monto { get; set; }
        public decimal pago_pendiente { get; set; }
    }
}