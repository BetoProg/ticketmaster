﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketMaster.Models;

namespace TicketMaster.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ShopDb db = new ShopDb();
        public TicketsController()
        {

        }

        public ActionResult Index()
        {
            var tickets = db.Database.SqlQuery<PaymentTicketViewModel> ("select pt.tic_id, pt.cus_id as cliente, isnull(sum(pp.PAY_TOTAL),0) as Monto, pt.TIC_TOTALAMOUNT as total_monto,"
            +" pt.TIC_TOTALAMOUNT - isnull(sum(pp.PAY_TOTAL), 0) as pago_pendiente"
            +" from pay_ticket as pt left join pay_payment pp"
            +" on pt.tic_id = pp.tic_id"
            +" group by pt.tic_id, pt.cus_id, pt.TIC_TOTALAMOUNT");

            return View(tickets);
        }
    }
}