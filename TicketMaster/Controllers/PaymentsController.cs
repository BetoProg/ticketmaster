﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketMaster.Models;

namespace TicketMaster.Controllers
{
    public class PaymentsController : Controller
    {
        private readonly ShopDb db = new ShopDb();

        public ActionResult Registrar()
        {
            ViewBag.TIC_ID = new SelectList(db.PAY_TICKET,"TIC_ID","TIC_ID");
            return View();
        }

        [HttpPost]
        public ActionResult Registrar(PAY_PAYMENT payment)
        {
            ViewBag.TIC_ID = new SelectList(db.PAY_TICKET, "TIC_ID", "TIC_ID");

            if (!ModelState.IsValid)
            {
                return View(payment);
            }

            payment.PAY_PAYMENTDATE = DateTime.Now;
            payment.PAY_CANCELED = 0;
            payment.CUR_ID = 1;

            db.PAY_PAYMENT.Add(payment);

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

            return RedirectToAction("Index","Tickets");
        }

        public ActionResult Delete(string id)
        {
            return View(db.PAY_TICKET.FirstOrDefault(x => x.TIC_ID == id));
        }
        
        [HttpPost]
        public ActionResult Delete(string tic_id,string id)
        {
            var pays = db.PAY_PAYMENT.Where(p => p.TIC_ID == tic_id).ToList();

            foreach(var pay in pays)
            {
                db.PAY_PAYMENT.Remove(pay);
            }

            try
            {

                db.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }

            return RedirectToAction("Index", "Tickets");
        }
    }
}